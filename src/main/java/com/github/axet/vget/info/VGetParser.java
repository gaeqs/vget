package com.github.axet.vget.info;

import com.github.axet.vget.info.VideoInfo.States;
import com.github.axet.wget.info.ex.DownloadInterruptedError;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

public abstract class VGetParser {

	public abstract VideoInfo info(URL web);

	public void info(VideoInfo info, AtomicBoolean stop, Runnable notify) {
		try {
			List<VideoFileInfo> dinfo = extract(info, stop, notify);
			List<VideoFileInfo> newInfo = new ArrayList<>();

			System.out.println(dinfo.size());
			for (VideoFileInfo i : dinfo) {
				try {
					i.setReferer(info.getWeb());
					i.extract(stop, notify);
					newInfo.add(i);
				} catch (Exception ex) {
					System.err.println("Error parsing option " + i.getContentType());
				}
			}

			info.setInfo(newInfo);
		} catch (DownloadInterruptedError e) {
			info.setState(States.STOP, e);
			notify.run();
			throw e;
		} catch (RuntimeException e) {
			info.setState(States.ERROR, e);
			notify.run();
			throw e;
		}
	}

	public abstract List<VideoFileInfo> extract(final VideoInfo vinfo, final AtomicBoolean stop, final Runnable notify);

}
